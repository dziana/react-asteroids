import React from 'react';

import {NasaService} from '../services/nasa-service';
export const ApiContext = React.createContext(new NasaService());
