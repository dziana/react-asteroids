import {useEffect, useState} from 'react'

const initialState = {
    data: null,
    loading: true,
    error: ''
}

export const useData = (request) => {
    const [dataState, setDataState] = useState(initialState)
    useEffect(() => {
        let cancelled = false
        setDataState(initialState)
        request()
            .then((data) => !cancelled ? setDataState({
                data: data.data,
                loading: false,
                error: ''
            }) : null)
            .catch((e) => !cancelled ? setDataState({
                data: null,
                loading: false,
                error: e.message
            }) : null)

        return () => {
            cancelled = true
        }
    }, [request])

    return dataState
}
