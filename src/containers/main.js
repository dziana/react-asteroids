import React from 'react';
import styled from 'styled-components'

import Form from "../components/form-component/form";
import List from "../components/list-component/list";
import {useSelector} from "react-redux";

const MainContainer = styled.div`
  width: 100%;
  background-color: black;
  padding: 20px;
  min-height: calc(100vh - 40px);
`;

const Title = styled.h1`
  margin: 0;
  padding: 10px;
  font-size: 2em;
  text-align: center;
  color: white;
`;

const Main = () => {
    const dateList = useSelector(state => state.dateList);

    return (
        <MainContainer>
            <Title>Near Earth asteroids</Title>
            {
                <Form>
                </Form>
            }
            { dateList &&
                <List>
                </List>
            }
        </MainContainer>
    );
}

export default Main;
