import {Provider} from "react-redux";
import {BrowserRouter as Router, Redirect, Route, Switch} from "react-router-dom";
import React from "react";
import styled from 'styled-components'

import store from "../store/store";
import Main from "./main";
import Detail from "../components/detailed-component/detailed";


const Container = styled.div`
  width: 80%;
  margin: 0 auto;
`;

const App = () => {
    return (
        <Provider store={store}>
            <Container>
                <Router>
                    <Switch>
                        <Redirect exact from="/" to="/main"/>
                        <Route path="/main" component={Main}/>
                        <Route path="/detail/:lightId" component={Detail}/>
                    </Switch>
                </Router>
            </Container>
        </Provider>
    );
}

export default App;
