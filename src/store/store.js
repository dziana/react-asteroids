import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootReducer from './reducers/reducer';
import {ApiContext} from "../contexts/ApiContext";


import rootSaga from './sagas'

export const sagaMiddleware = createSagaMiddleware({
    context: {
        api: ApiContext
    }
});

export default createStore(
    rootReducer,
    applyMiddleware(sagaMiddleware)
);

sagaMiddleware.run(rootSaga);
