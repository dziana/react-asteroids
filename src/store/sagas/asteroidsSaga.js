import { put, call, getContext } from 'redux-saga/effects';
import {
    loadDateListError,
    loadDateListSuccess
} from '../actions/action';

export function* asteroidsSaga(payload) {
    const api = yield getContext('api')
    try {
        const response = yield call(api._currentValue.getAllAsteroids, payload.dates);
        yield put(loadDateListSuccess({
            dateList: response.data.near_earth_objects,
            dateTo: payload.dates.dateTo,
            dateFrom: payload.dates.dateFrom
        }))
    } catch(error) {
        yield put(loadDateListError());
    }
}
