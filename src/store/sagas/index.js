import { fork } from 'redux-saga/effects';
import watchAsteroids from './watcher';

export default function* startForman() {
    yield fork(watchAsteroids);
}
