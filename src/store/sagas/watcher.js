import { takeLatest } from 'redux-saga/effects';
import { asteroidsSaga } from './asteroidsSaga';

import * as types from '../actions/action';

export default function* watchAsteroids() {
    yield takeLatest(types.LOAD_DATE_LIST, asteroidsSaga);
}
