export const UPDATE_SELECTED_DATE = 'UPDATE_SELECTED_DATE';
export const UPDATE_DATE_FROM = 'UPDATE_DATE_FROM';
export const UPDATE_DATE_TO = 'UPDATE_DATE_TO';
export const UPDATE_LIST = 'UPDATE_LIST';
export const LOAD_DATE_LIST = 'LOAD_DATE_LIST';
export const LOAD_DATE_LIST_SUCCESS = 'LOAD_DATE_LIST_SUCCESS';
export const LOAD_DATE_LIST_ERROR = 'LOAD_DATE_LIST_ERROR';

export function updateSelectedDate(selectedDate) {
    return {type: UPDATE_SELECTED_DATE, selectedDate};
}

export function updateDateFrom(dateFrom) {
    return {type: UPDATE_DATE_FROM, dateFrom};
}

export function updateDateTo(dateTo) {
    return {type: UPDATE_DATE_TO, dateTo};
}

export function updateDateList(dateList) {
    return {type: UPDATE_LIST, dateList};
}

export function loadDateList(dates) {
    return {type: LOAD_DATE_LIST, dates};
}

export function loadDateListSuccess(data) {
    return {type: LOAD_DATE_LIST_SUCCESS, data};
}

export function loadDateListError() {
    return {type: LOAD_DATE_LIST_ERROR};
}
