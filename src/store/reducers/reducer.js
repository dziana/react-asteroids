
import {LOAD_DATE_LIST_SUCCESS, LOAD_DATE_LIST_ERROR, UPDATE_SELECTED_DATE, UPDATE_DATE_FROM, UPDATE_DATE_TO, UPDATE_LIST, LOAD_DATE_LIST} from '../actions/action';

const initialState = {
    selectedDate: '',
    dateFrom: '',
    dateTo: '',
    dateList: null,
    dateListLoading: false,
    dateListError: false
};

function rootReducer(state = initialState, action) {
    switch(action.type) {
        case UPDATE_SELECTED_DATE:
            return {
                ...state,
                selectedDate: action.selectedDate
            };
        case UPDATE_DATE_FROM:
            return {
                ...state,
                dateFrom: action.dateFrom
            };
        case UPDATE_DATE_TO:
            return {
                ...state,
                dateTo: action.dateTo
            };
        case UPDATE_LIST:
            return {
                ...state,
                dateList: action.dateList
            };
        case LOAD_DATE_LIST:
            return {
                ...state,
                dateList: null,
                dateListLoading: true,
                dateListError: false
            };
        case LOAD_DATE_LIST_SUCCESS:
            return {
                ...state,
                dateList: action.data.dateList,
                dateFrom: action.data.dateFrom,
                dateTo: action.data.dateTo,
                dateListLoading: false
            };
        case LOAD_DATE_LIST_ERROR:
            return {
                ...state,
                dateListLoading: false,
                dateListError: true
            };
        default:
            return state;
    }
}

export default rootReducer;
