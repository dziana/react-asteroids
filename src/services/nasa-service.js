import axios from "axios";

export class NasaService {

    apiKey = '9Ix4GmtXb3ZOtISXbhA91egYf7rFwo1s4cZoqbq3';

    getAllAsteroids = async (dates) =>
        await axios.get(
            `https://api.nasa.gov/neo/rest/v1/feed?start_date=${dates.dateFrom}&end_date=${dates.dateTo}&api_key=${this.apiKey}`
        )

    getOneAsteroid = async (id) =>
        await axios.get(
            `https://api.nasa.gov/neo/rest/v1/neo/${id}?api_key=${this.apiKey}`
        )
}
