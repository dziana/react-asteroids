import React, {useCallback} from 'react';
import {Link} from "react-router-dom";
import styled from "styled-components";

import Spinner from "../spinner-component/spinner";
import withNasaService from "../../wrapper/withNasaService";
import {useData} from "../../hooks/useData";

const DataBlock = styled.div`
  width: 60%;
`;

const DetailedContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 100vh;
  width: 100%;
  background-color: black;
`;

const Line = styled.div`
  display: flex;
  width: 100%;
  background-color: white;
  border-radius: 5px;
  margin-bottom: 10px;

  & p {
    flex: 1;
    background-color: white;
    padding: 10px;
    margin: 10px 5px;
    word-break: break-word;
  }
`;

const BackLink = styled(Link)`
    &:link, &:visited {
      color: white;
    }
`;

const Row = ({name, value}) => {
    return (
        <Line className="line">
            <p>{name}:</p>
            <p>{value}</p>
        </Line>
    );
}

const Detail = ({data: {name, absolute_magnitude_h, estimated_diameter}}) => {
    const {estimated_diameter_min, estimated_diameter_max} = estimated_diameter?.meters;
    return (
        <DataBlock>
            <p><BackLink to={'/main'}>Back</BackLink></p>
            <Row name="Name" value={name}/>
            <Row name="Absolute magnitude h" value={absolute_magnitude_h}/>
            <Row name="Estimated diameter" value={`${estimated_diameter_min} ... ${estimated_diameter_max}`}/>
        </DataBlock>
    );
}

export const DetailContainer = ({nasaService, match: {params: {lightId}}}) => {

    const query = useCallback(() => nasaService.getOneAsteroid(lightId), [lightId, nasaService])
    const state = useData(query)
    return (
        <DetailedContainer>
            {state.loading && <Spinner/>}
            {state.error && <p>{state.error}</p>}
            {state.data && <Detail data={state.data}/>}
        </DetailedContainer>
    )
};
export default withNasaService(DetailContainer);
