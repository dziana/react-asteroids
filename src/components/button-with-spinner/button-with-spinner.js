import React from "react";
import styled from "styled-components";

import './button-with-spinner.css';

const Error = styled.span`
  color: #e74c3c;
  margin-top: 2px;
`;

const Button = styled.div`
  margin-top: 19px;
  display: flex;
  background-color: #375a7f;
  padding: 8px 10px;
  border-radius: 5px;
  width: fit-content;
  
  &:focus-within {
    outline: 1px auto white;
  }

  input {
    color: white;
    border: 0;
    background-color: transparent;
    outline: 0;
    cursor: pointer;
  }
`;


const SpinnerForButton = () => {
    return (
        <div className="spinner-ring">
            <div/>
            <div/>
            <div/>
            <div/>
        </div>
    );
};

const ButtonWithSpinnerAndError = ({loading, error}) => {
    return (
        <React.Fragment>
            <Button>
                <input
                    type='submit'
                    className="form-item"
                    value='Get'
                />
                {loading && <SpinnerForButton/>}
            </Button>
            {error && <Error>{error}</Error>}
        </React.Fragment>
    );
};

export default ButtonWithSpinnerAndError;
