import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {useForm} from "react-hook-form";
import styled from "styled-components";

import {loadDateList} from "../../store/actions/action";
import ButtonWithSpinnerAndError from "../button-with-spinner/button-with-spinner";

const _MS_PER_DAY = 1000 * 60 * 60 * 24;

const StyledForm = styled.form`
  margin-top: 10px;
  padding: 10px;
  display: flex;
  flex-wrap: wrap;
`;

const FormItem = styled.div`
  display: flex;
  flex-direction: column;
  margin: 5px;
`;

const Label = styled.label`
  color: white;
  margin-bottom: 2px;
`;

const Input = styled.input`
  background-color: white;
  padding: 6px 10px;
  border: none;
  border-radius: 5px;
`;

const Error = styled.span`
  color: #e74c3c;
  margin-top: 2px;
`;

export const Form = () => {
    const storeDateFrom = useSelector(state => state.dateFrom);
    const storeDateTo = useSelector(state => state.dateTo);
    const loading = useSelector(state => state.dateListLoading);
    const listError = useSelector(state => state.dateListError);
    const dispatch = useDispatch();

    const {register, handleSubmit, errors} = useForm();
    const [error, setError] = useState('');

    const onSubmit = (data) => {
        const day1 = new Date(data.dateFrom);
        const day2 = new Date(data.dateTo);
        const difference = dateDiffInDays(day1, day2);
        if (difference > 7 || difference < 0) {
            setError('Time interval should be less than 7 days');
            return;
        }
        const {dateFrom, dateTo} = data
        dispatch(loadDateList({
            dateFrom,
            dateTo
        }))
    }

    const dateDiffInDays = (a, b) => {
        const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
        const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
        return Math.floor((utc2 - utc1) / _MS_PER_DAY);
    }
    return (
        <StyledForm onSubmit={handleSubmit(onSubmit)}>
            <FormItem>
                <Label>From:</Label>
                <Input type="date" defaultValue={storeDateFrom} name='dateFrom' ref={register({required: true})}/>
                {errors.dateFrom && <Error>This field is required</Error>}
            </FormItem>
            <FormItem>
                <Label>To: </Label>
                <Input type="date" defaultValue={storeDateTo} name='dateTo' ref={register({required: true})}/>
                {errors.dateTo && <Error>This field is required</Error>}
            </FormItem>
            <FormItem>
                <ButtonWithSpinnerAndError loading={loading} error={error || listError}/>
            </FormItem>
        </StyledForm>
    );
};

export default Form;
