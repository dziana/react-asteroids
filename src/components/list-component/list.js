import React from 'react';
import {Link} from "react-router-dom";
import {useSelector, useDispatch} from 'react-redux'
import styled from 'styled-components'
import moment from 'moment';

import {updateSelectedDate} from "../../store/actions/action";

const DateContainer = styled.ul`
  display: flex;
  padding: 10px;
  flex-wrap: wrap;
  align-items: center;
`;

const DateItem = styled.li`
  background-color: white;
  margin: 5px;
  cursor: pointer;
  border-radius: 5px;
  border: ${props => props.active ? '3px solid #f39c12' : 'none'};
  list-style-type: none;
`;

const DateButton = styled.button`
  background-color: transparent;
  padding: 5px;
  border: none;
  cursor: pointer;
`;

const StyledAsteroidList = styled.div`
  width: 50%;
  margin: 0 10px 20px;
`;

const AsteroidItem = styled.div`
  margin: 5px;
  padding: 10px;
  border-radius: 5px;
  background-color: white;
`;

const StyledLink = styled(Link)`
  &:link, &:visited {
    color: black;
  }
`;
const AsteroidList = ({asteroidList}) => {
    const asteroidRow = (light) => {
        return (
            <StyledLink to={`/detail/${light.id}`} key={light.id} className="light">
                <AsteroidItem>
                    {light.name}
                </AsteroidItem>
            </StyledLink>
        );
    };

    return (
        <StyledAsteroidList className="item-value">
            {
                asteroidList.map(asteroidRow)
            }
        </StyledAsteroidList>
    )
}

export const DateList = ({dateList, selectedDate, selectDate}) => {
    return (
        <DateContainer>
            {
                Object.keys(dateList).map(
                    date => (
                        <DateItem key={'key' + date}
                                  active={selectedDate === date}
                        >
                            <DateButton
                                onClick={() => {
                                    selectDate(date)
                                }}
                            >
                                {moment(date, 'YYYY-MM-DD').format('DD.MM.YYYY')}
                            </DateButton>
                        </DateItem>)
                )
            }
        </DateContainer>
    );
};

export function List() {

    const selectedDate = useSelector(state => state.selectedDate);
    const dateList = useSelector(state => state.dateList);
    const dispatch = useDispatch();

    const selectDate = (date) => {
        dispatch(updateSelectedDate(date));
    }

    return (
        <React.Fragment>
            <DateList selectDate={selectDate} dateList={dateList} selectedDate={selectedDate}/>
            {
                dateList[selectedDate]?.length && <AsteroidList asteroidList={dateList[selectedDate]}/>
            }
        </React.Fragment>
    )
}

export default List;
