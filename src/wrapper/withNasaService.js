import React, {useContext} from "react";
import {ApiContext} from "../contexts/ApiContext";

function withNasaService(WrappedComponent) {
    return function (props) {
        const nasaService = useContext(ApiContext);
        return <WrappedComponent nasaService={nasaService} {...props}/>
    }
}

export default withNasaService;
